# !/usr/bin/env python
"""Usage:

    >>> python findlatlng.py Some address
    address: XXXXX, latitude: ####, longitude: ####
    address: XXXXX, latitude: ####, longitude: ####
    address: XXXXX, latitude: ####, longitude: ####
    ...
"""
import urllib
import urllib2
import json

OK = 'OK'
api_url = "http://maps.googleapis.com/maps/api/geocode/json"

def get_latlng(**options):
    """Use google maps API to find the latitude and longitude of the given
    address.
    Any keyword argument is automatically passed as a GET param to the maps
    service.
    example:

        get_latlng(sensor='false', address='My Address')
    """
    results = []
    params = urllib.urlencode(options)
    if params:
        url = '%s?%s' % (api_url, params)
    else:
        url = api_url
    try:
        response = urllib2.urlopen(url)
    except urllib2.URLError, e:
        raise
    else:
        data = json.loads(response.read())
        if data['status'] == OK:
            for result in data['results']:
                results.append((result['formatted_address'], result['geometry']['location']['lat'], result['geometry']['location']['lng']))
    return results


def main():
    results = get_latlng(sensor='false', address='Casimiro Escudero 2 Madrid')
    for result in results:
        print 'address: %s, latitude: %s, longitude: %s' % result

if __name__ == '__main__':
    main()

