Find latitude and longitude for some address
=======================================


Command line program for easily find the latitude and longitude for a given address using google maps API.
Example:

		>>> python findlatlng.py Some address
		address: XXXXX, latitude: ####, longitude: ####
		address: XXXXX, latitude: ####, longitude: ####
		address: XXXXX, latitude: ####, longitude: ####
		...

The core of the module is the get_latlng() function which can be imported and used independently.
Example:
		
		>>> from findlatlng import get_latlng
		>>> get_latlng(sensor='false', address='My Address')
		[(<Google Address 1>, <latitude>, <longitude>), ...]
